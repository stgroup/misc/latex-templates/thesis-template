[Latest PDF online](/../-/jobs/artifacts/main/file/thesis.pdf?job=build) ([Build Log](/../-/jobs/artifacts/main/file/thesis.log?job=build))

Helpful documents
* [Manual for the TU LaTeX template](http://mirrors.ctan.org/macros/latex/contrib/tudscr/doc/tudscr.pdf)
* [Ein Anwenderleitfaden für das Erstellen einer wissenschaftlichen Abhandlung](http://mirrors.ctan.org/macros/latex/contrib/tudscr/doc/tutorials/treatise.pdf)
